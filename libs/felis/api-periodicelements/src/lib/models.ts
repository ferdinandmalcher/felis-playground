export interface PeriodicElement {
  id: string;
  name: string;
  weight: number;
  symbol: string;
}
