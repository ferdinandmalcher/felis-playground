import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { CrossWindowEffects } from './+state/cross-window.effects';
import * as fromCrossWindow from './+state/cross-window.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromCrossWindow.CROSSWINDOW_FEATURE_KEY,
      fromCrossWindow.reducer
    ),
    EffectsModule.forFeature([CrossWindowEffects]),
  ],
})
export class CrossWindowDataAccessModule {}
