import { TestBed } from '@angular/core/testing';

import { MonitorEntryGuard } from './monitor-entry.guard';

describe('MonitorEntryGuard', () => {
  let guard: MonitorEntryGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MonitorEntryGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
