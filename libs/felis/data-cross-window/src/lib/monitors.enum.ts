export enum Monitors {
  Main = 'main',
  Secondary = 'secondary',
  Tertiary = 'tertiary',
}
