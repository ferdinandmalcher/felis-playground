import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';

import * as CrossWindowActions from './cross-window.actions';
import { CrossWindowService } from '../cross-window.service';

@Injectable()
export class CrossWindowEffects {
  openWindows$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(CrossWindowActions.openApplicationWindows),
        tap(() => this.cws.openApplicationWindows())
      ),
    { dispatch: false }
  );

  constructor(private actions$: Actions, private cws: CrossWindowService) {}
}
