import { createAction, props } from '@ngrx/store';

export const openApplicationWindows = createAction(
  '[CrossWindow] Open Application Windows'
);
