import { createReducer, on, Action } from '@ngrx/store';
import * as CrossWindowActions from './cross-window.actions';

export const CROSSWINDOW_FEATURE_KEY = 'crossWindow';

export interface State {
  currentMonitorName: string;
}

export interface CrossWindowPartialState {
  readonly [CROSSWINDOW_FEATURE_KEY]: State;
}

export const initialState: State = {
  currentMonitorName: null,
};

const crossWindowReducer = createReducer(initialState);

export function reducer(state: State | undefined, action: Action) {
  return crossWindowReducer(state, action);
}
