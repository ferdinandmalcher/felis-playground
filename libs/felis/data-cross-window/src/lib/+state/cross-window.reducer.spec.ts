import * as CrossWindowActions from './cross-window.actions';
import { State, initialState, reducer } from './cross-window.reducer';

describe('CrossWindow Reducer', () => {
  beforeEach(() => {});

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
