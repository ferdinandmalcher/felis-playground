import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  CROSSWINDOW_FEATURE_KEY,
  State,
  CrossWindowPartialState,
} from './cross-window.reducer';

export const getCrossWindowState = createFeatureSelector<
  CrossWindowPartialState,
  State
>(CROSSWINDOW_FEATURE_KEY);

export const getCurrentMonitorName = createSelector(
  getCrossWindowState,
  state => state.currentMonitorName
);
