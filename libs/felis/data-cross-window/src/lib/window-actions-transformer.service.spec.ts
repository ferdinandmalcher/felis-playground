import { Injectable } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Actions, EffectsModule } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, createAction, props } from '@ngrx/store';
import { of, Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { CrossWindowService } from './cross-window.service';

import {
  createTransformDefinition,
  WindowActionsTransformer,
} from './window-actions-transformer.service';

// Test actions
const testSourceActionOne = createAction('[Test] Source One');
const testSourceActionTwo = createAction(
  '[Test] Source Two',
  props<{ data: string }>()
);
const testSourceActionThree = createAction('[Test] Source Three');

const testResultActionOne = createAction('[Test] Result One');
const testResultActionTwo = createAction(
  '[Test] Result Two',
  props<{ result: string }>()
);

// Effects class with inbound and outbound effects for testing the util functions
@Injectable()
export class TestEffects {
  private inboundTransformations = [
    createTransformDefinition({
      from: testSourceActionOne,
      to: () => testResultActionOne(),
    }),
    createTransformDefinition({
      from: testSourceActionTwo,
      to: action => testResultActionTwo({ result: action.data }),
    }),
  ];

  inboundActions$ = this.wat.createInboundTransformationEffect(
    this.inboundTransformations
  );

  outboundActions$ = this.wat.createOutboundBroadcastEffect([
    testSourceActionOne,
    testSourceActionTwo,
  ]);

  constructor(private wat: WindowActionsTransformer) {}
}

describe('WindowActionsTransformer', () => {
  let actions$: Actions;
  let effects: TestEffects;
  let inboundMessages$: Subject<Action>;
  let crossWindowServiceMock: Partial<CrossWindowService>;

  beforeEach(() => {
    // mocked stream of inbound messages. can be emitted from the tests
    inboundMessages$ = new Subject();

    // mock object for CrossWindowService
    crossWindowServiceMock = {
      inboundBroadcastActions$: inboundMessages$.asObservable(),
      broadcastAction: jest.fn(),
    };
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TestEffects,
        provideMockActions(() => actions$),
        { provide: CrossWindowService, useValue: crossWindowServiceMock },
      ],
    });
    effects = TestBed.inject(TestEffects);
  });

  describe('inbound action transformation', () => {
    let dispatchedAction;

    beforeEach(() => {
      dispatchedAction = undefined;
      effects.inboundActions$.subscribe(action => (dispatchedAction = action));
    });

    it('should transform incoming broadcasted actions based on the definitions', () => {
      inboundMessages$.next(testSourceActionOne());

      expect(dispatchedAction).toEqual(testResultActionOne());
    });

    it('should transform incoming actions with payload', () => {
      const data = 'This is a test.';
      inboundMessages$.next(testSourceActionTwo({ data }));

      expect(dispatchedAction).toEqual(testResultActionTwo({ result: data }));
    });

    it('should not transform actions that do not have transform definitions', () => {
      inboundMessages$.next(testSourceActionThree());

      expect(dispatchedAction).toBeUndefined();
    });
  });

  describe('outbound action broadcast', () => {
    it('should broadcast all actions defined in the broadcast list', () => {
      const actionOne = testSourceActionOne();
      const actionTwo = testSourceActionTwo({ data: '' });

      actions$ = of(actionOne, actionTwo);
      effects.outboundActions$.subscribe();

      expect(crossWindowServiceMock.broadcastAction).toHaveBeenNthCalledWith(
        1,
        actionOne
      );
      expect(crossWindowServiceMock.broadcastAction).toHaveBeenNthCalledWith(
        2,
        actionTwo
      );
    });

    it('should not broadcast any other actions', () => {
      actions$ = of(testSourceActionThree());
      effects.outboundActions$.subscribe();

      expect(crossWindowServiceMock.broadcastAction).not.toHaveBeenCalled();
    });
  });
});
