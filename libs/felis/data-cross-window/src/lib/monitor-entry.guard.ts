import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  UrlTree,
  Router,
} from '@angular/router';

import { Monitors } from './monitors.enum';

@Injectable({
  providedIn: 'root',
})
export class MonitorEntryGuard implements CanActivate {
  /**
   * entry routes for each monitor name
   */
  private redirectMap = {
    [Monitors.Main]: '/elements/list',
    [Monitors.Secondary]: '/elements',
    [Monitors.Tertiary]: '/elements',
  };

  constructor(private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot): UrlTree {
    const monitorName = next.paramMap.get('monitorName');
    const path = this.redirectMap[monitorName] || '/';

    return this.router.parseUrl(path);
  }
}
