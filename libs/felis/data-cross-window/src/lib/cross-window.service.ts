import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { Action } from '@ngrx/store';

import { Monitors } from './monitors.enum';
import { BroadcastChannelService, WINDOW } from '@els/shared/util-browser-apis';

/**
 * internal name of the broadcast channel between all windows
 */

@Injectable({
  providedIn: 'root',
})
export class CrossWindowService {
  /**
   * List of monitor names that should be opened by default
   */
  private monitorsToOpen: Monitors[] = [
    Monitors.Main,
    Monitors.Secondary,
    Monitors.Tertiary,
  ];

  /**
   * Default options for window.open
   */
  private defaultWindowOptions = 'width=600,height=800';

  /**
   * Stream of all inbound messages from other windows.
   * Can be consumed in effects to react to these broadcasted actions.
   */
  readonly inboundBroadcastActions$: Observable<Action>;

  constructor(
    @Inject(WINDOW) private window: Window,
    private channel: BroadcastChannelService,
    private router: Router
  ) {
    this.inboundBroadcastActions$ = this.channel.onMessage$.pipe(
      map(e => e.data),
      share()
    );
  }

  /**
   * Open all monitors for the application, as defined in {@link monitorsToOpen}
   */
  openApplicationWindows(
    monitorsToOpen: Monitors[] = this.monitorsToOpen,
    options?: string
  ) {
    const isMainWindow = this.window.opener === null; // TODO: How can we determine the main window?
    if (!isMainWindow) {
      console.log(`This is a sub monitor with name "${this.window.name}".`); // DEBUG
      return;
    }

    monitorsToOpen
      .filter(m => m !== Monitors.Main)
      .forEach(monitorName => {
        console.log(`Opening monitor "${monitorName}"`); // DEBUG

        this.window.open(
          `/monitor/${monitorName}`,
          monitorName,
          options || this.defaultWindowOptions
        );
      });

    // use the current window as main monitor
    console.log('This is the main monitor.'); // DEBUG
    this.window.name = Monitors.Main;
    this.router.navigate(['/monitor', Monitors.Main]);
  }

  /**
   * Broadcast a message to all other windows
   * @param message Action to be broadcasted
   */
  broadcastAction(message: Action) {
    this.channel.postMessage(message);
  }
}
