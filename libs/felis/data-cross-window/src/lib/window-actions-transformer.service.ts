import { ActionCreator, Action } from '@ngrx/store';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, tap } from 'rxjs/operators';
import { pipe } from 'rxjs';
import { Injectable } from '@angular/core';
import { CrossWindowService } from './cross-window.service';

interface ActionTransformDefinition<
  A extends ActionCreator = ActionCreator,
  R = ReturnType<A>
> {
  from: A;
  to: (action: R) => Action;
}

interface TransformDefinitionsObject<
  A extends ActionCreator,
  R = ReturnType<A>
> {
  [type: string]: ActionTransformDefinition<A, R>;
}

/**
 * Helper function to create a correctly typed inbound action transformation definition
 * @param def An transformation definition
 */
export function createTransformDefinition<
  C extends ActionCreator,
  R = ReturnType<C>
>(def: ActionTransformDefinition<C, R>): ActionTransformDefinition<C, R> {
  return def;
}

@Injectable({ providedIn: 'root' })
export class WindowActionsTransformer {
  constructor(private cws: CrossWindowService, private actions$: Actions) {}

  /**
   * Creates an effect that receives inbound broadcast actions and transforms them into new NgRx actions
   * @param defs Array of transformation definitions that define how inbound actions should be transformed into new actions
   */
  createInboundTransformationEffect(
    defs: ActionTransformDefinition<any, any>[]
  ) {
    return createEffect(() =>
      this.cws.inboundBroadcastActions$.pipe(this.transformActions(defs))
    );
  }

  /**
   * Creates an effect that broadcasts actions to other windows
   * @param broadcastedActions Array of actions from this feature that should be broadcasted to other windows
   */
  createOutboundBroadcastEffect(broadcastedActions: ActionCreator[]) {
    return createEffect(
      () =>
        this.actions$.pipe(
          ofType(...broadcastedActions),
          tap((action: Action) => this.cws.broadcastAction(action))
        ),
      { dispatch: false }
    );
  }

  /**
   * Helper function that works as an operator to create the inbound transformation effect
   * @param defs Array of transformation definitions
   */
  private transformActions(defs: ActionTransformDefinition<any, any>[]) {
    const defObj = this.transformDefinitionsToObject(defs);
    const creators = defs.map(d => d.from);

    return pipe(
      ofType(...creators),
      map(action => {
        const def = defObj[action.type];
        if (typeof def.to === 'function') {
          return def.to(action);
        }
      })
    );
  }

  /**
   * Helper function that converts an array of transformation defs to an object for fast lookup
   * @param defs Array of transformation definitions
   */
  private transformDefinitionsToObject<
    C extends ActionCreator,
    R = ReturnType<C>
  >(defs: ActionTransformDefinition<C, R>[]): TransformDefinitionsObject<C, R> {
    return defs.reduce((acc, def) => ({ ...acc, [def.from.type]: def }), {});
  }
}
