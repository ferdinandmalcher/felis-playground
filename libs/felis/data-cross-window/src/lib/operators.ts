import { filter } from 'rxjs/operators';
import { Monitors } from './monitors.enum';

/**
 * RxJS operator to filter by current monitor name.
 * The pipeline will only continue if it runs on one of the monitors specified as an argument.
 * @param monitors List of allowed monitor names
 */
export function onlyForMonitors(...allowedMonitors: Monitors[]) {
  const myName = window.name;
  return filter(
    () => myName && isValidMonitor(myName) && allowedMonitors.includes(myName)
  );
}

/**
 * Helper function to check whether a given monitor name is part of the Monitors enum
 * @param monitorName Monitor name as string
 */
function isValidMonitor(monitorName: string): monitorName is Monitors {
  return Object.values(Monitors).includes(monitorName as Monitors);
}
