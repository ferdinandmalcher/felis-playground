import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Location } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { EMPTY } from 'rxjs';
import { BroadcastChannelService, WINDOW } from '@els/shared/util-browser-apis';

import { CrossWindowService } from './cross-window.service';
import { Monitors } from './monitors.enum';

describe('CrossWindowService', () => {
  let service: CrossWindowService;
  let channelMock: Partial<BroadcastChannelService>;
  let windowMock: Partial<Window>;

  beforeEach(() => {
    // mock native browser object
    windowMock = {
      open: jest.fn(),
      opener: null,
      name: '',
    };

    channelMock = {
      postMessage: jest.fn(),
      onMessage$: EMPTY,
    };
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'monitor/:monitorName', children: [] },
        ]),
      ],
      providers: [
        { provide: BroadcastChannelService, useValue: channelMock },
        { provide: WINDOW, useValue: windowMock },
      ],
    });
    service = TestBed.inject(CrossWindowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open windows but skip the main monitor', () => {
    const options = 'foo=bar';

    const monitorsToOpen = [Monitors.Main, 'B', 'C'] as Monitors[];
    service.openApplicationWindows(monitorsToOpen, options);

    expect(windowMock.open).toHaveBeenNthCalledWith(
      1,
      '/monitor/B',
      'B',
      options
    );
    expect(windowMock.open).toHaveBeenNthCalledWith(
      2,
      '/monitor/C',
      'C',
      options
    );
  });

  it('should navigate the current monitor', fakeAsync(() => {
    const location = TestBed.inject(Location);

    const monitorsToOpen = [Monitors.Main, 'B', 'C'] as Monitors[];
    service.openApplicationWindows(monitorsToOpen);
    tick();
    expect(location.path()).toBe('/monitor/main');
  }));

  it('should forward broadcast calls to the Broadcast Channel', () => {
    const action = { type: 'FOO' };
    service.broadcastAction(action);

    expect(channelMock.postMessage).toHaveBeenCalledWith(action);
  });
});
