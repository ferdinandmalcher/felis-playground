import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElementListComponent } from './element-list/element-list.component';
import { RouterModule } from '@angular/router';
import { PeriodicelementsDataAccessModule } from '@els/felis/data-periodicelements';
import { ElementDetailsComponent } from './element-details/element-details.component';
import { StartComponent } from './start/start.component';
import { ElementDetailsBigComponent } from './element-details-big/element-details-big.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: StartComponent },
      { path: 'list', component: ElementListComponent },
      { path: 'details/:id', component: ElementDetailsComponent },
      { path: 'detailsbig/:id', component: ElementDetailsBigComponent },
    ]),
    PeriodicelementsDataAccessModule,
  ],
  declarations: [
    ElementListComponent,
    ElementDetailsComponent,
    StartComponent,
    ElementDetailsBigComponent,
  ],
})
export class FelisFeaturePeriodicelementsModule {}
