import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementDetailsBigComponent } from './element-details-big.component';

describe('ElementDetailsBigComponent', () => {
  let component: ElementDetailsBigComponent;
  let fixture: ComponentFixture<ElementDetailsBigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementDetailsBigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementDetailsBigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
