import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { getSelectedElement } from '@els/felis/data-periodicelements';

@Component({
  selector: 'felis-pe-element-details-big',
  templateUrl: './element-details-big.component.html',
  styleUrls: ['./element-details-big.component.scss'],
})
export class ElementDetailsBigComponent implements OnInit {
  elem$ = this.store.pipe(select(getSelectedElement));

  constructor(private store: Store) {}

  ngOnInit(): void {}
}
