import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { getSelectedElement } from '@els/felis/data-periodicelements';

@Component({
  selector: 'felis-pe-element-details',
  templateUrl: './element-details.component.html',
  styleUrls: ['./element-details.component.scss'],
})
export class ElementDetailsComponent implements OnInit {
  elem$ = this.store.pipe(select(getSelectedElement));

  constructor(private store: Store) {}

  ngOnInit(): void {}
}
