import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  loadPeriodicelements,
  getAllPeriodicelements,
  PeriodicElement,
  selectPeriodicelement,
} from '@els/felis/data-periodicelements';

@Component({
  selector: 'els-element-list',
  templateUrl: './element-list.component.html',
  styleUrls: ['./element-list.component.scss'],
})
export class ElementListComponent implements OnInit {
  elements$ = this.store.pipe(select(getAllPeriodicelements));

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(loadPeriodicelements());
  }

  selectElement(elem: PeriodicElement) {
    this.store.dispatch(selectPeriodicelement({ id: elem.id }));
  }
}
