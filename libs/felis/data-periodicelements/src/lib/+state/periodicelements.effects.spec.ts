import { TestBed, async } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { NxModule, DataPersistence } from '@nrwl/angular';
import { hot } from '@nrwl/angular/testing';

import { PeriodicelementsEffects } from './periodicelements.effects';
import * as PeriodicelementsActions from './periodicelements.actions';

describe('PeriodicelementsEffects', () => {
  let actions: Observable<any>;
  let effects: PeriodicelementsEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        PeriodicelementsEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore(),
      ],
    });

    effects = TestBed.get(PeriodicelementsEffects);
  });

  describe('loadPeriodicelements$', () => {
    it('should work', () => {
      actions = hot('-a-|', {
        a: PeriodicelementsActions.loadPeriodicelements(),
      });

      const expected = hot('-a-|', {
        a: PeriodicelementsActions.loadPeriodicelementsSuccess({
          periodicelements: [],
        }),
      });

      expect(effects.loadPeriodicelements$).toBeObservable(expected);
    });
  });
});
