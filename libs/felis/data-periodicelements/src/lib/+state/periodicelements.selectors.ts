import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  PERIODICELEMENTS_FEATURE_KEY,
  State,
  PeriodicelementsPartialState,
  periodicelementsAdapter,
} from './periodicelements.reducer';
import { selectRouter, selectRouteParam } from '@els/shared/util-ngrx-router';

// Lookup the 'Periodicelements' feature state managed by NgRx
export const getPeriodicelementsState = createFeatureSelector<
  PeriodicelementsPartialState,
  State
>(PERIODICELEMENTS_FEATURE_KEY);

const { selectAll, selectEntities } = periodicelementsAdapter.getSelectors();

export const getPeriodicelementsLoaded = createSelector(
  getPeriodicelementsState,
  (state: State) => state.loaded
);

export const getPeriodicelementsError = createSelector(
  getPeriodicelementsState,
  (state: State) => state.error
);

export const getAllPeriodicelements = createSelector(
  getPeriodicelementsState,
  (state: State) => selectAll(state)
);

export const getPeriodicelementsEntities = createSelector(
  getPeriodicelementsState,
  (state: State) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getPeriodicelementsState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getPeriodicelementsEntities,
  getSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);

export const getSelectedElementId = selectRouteParam('id');

export const getSelectedElement = createSelector(
  getPeriodicelementsEntities,
  getSelectedElementId,
  (entities, id) => entities[id]
);
