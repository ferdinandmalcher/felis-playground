import { PeriodicelementsEntity } from './periodicelements.models';
import {
  State,
  periodicelementsAdapter,
  initialState,
} from './periodicelements.reducer';
import * as PeriodicelementsSelectors from './periodicelements.selectors';

describe('Periodicelements Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getPeriodicelementsId = it => it['id'];
  const createPeriodicelementsEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as PeriodicelementsEntity);

  let state;

  beforeEach(() => {
    state = {
      periodicelements: periodicelementsAdapter.addAll(
        [
          createPeriodicelementsEntity('PRODUCT-AAA'),
          createPeriodicelementsEntity('PRODUCT-BBB'),
          createPeriodicelementsEntity('PRODUCT-CCC'),
        ],
        {
          ...initialState,
          selectedId: 'PRODUCT-BBB',
          error: ERROR_MSG,
          loaded: true,
        }
      ),
    };
  });

  describe('Periodicelements Selectors', () => {
    it('getAllPeriodicelements() should return the list of Periodicelements', () => {
      const results = PeriodicelementsSelectors.getAllPeriodicelements(state);
      const selId = getPeriodicelementsId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelected() should return the selected Entity', () => {
      const result = PeriodicelementsSelectors.getSelected(state);
      const selId = getPeriodicelementsId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it("getPeriodicelementsLoaded() should return the current 'loaded' status", () => {
      const result = PeriodicelementsSelectors.getPeriodicelementsLoaded(state);

      expect(result).toBe(true);
    });

    it("getPeriodicelementsError() should return the current 'error' state", () => {
      const result = PeriodicelementsSelectors.getPeriodicelementsError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
