import { createAction, props } from '@ngrx/store';
import { PeriodicElement } from '@els/felis/api-periodicelements';

export const loadPeriodicelements = createAction(
  '[Periodicelements] Load Periodicelements'
);

export const loadPeriodicelementsSuccess = createAction(
  '[Periodicelements] Load Periodicelements Success',
  props<{ periodicelements: PeriodicElement[] }>()
);

export const loadPeriodicelementsFailure = createAction(
  '[Periodicelements] Load Periodicelements Failure',
  props<{ error: any }>()
);

export const selectPeriodicelement = createAction(
  '[Periodicelements] Select Periodicelement',
  props<{ id: string }>()
);

export const selectPeriodicelementBroadcast = createAction(
  '[Broadcast Periodicelements] Select Periodicelement',
  props<{ id: string }>()
);

export const loadPeriodicelement = createAction(
  '[Periodicelements] Load Periodicelement',
  props<{ id: string }>()
);

export const loadPeriodicelementSuccess = createAction(
  '[Periodicelements] Load Periodicelement Success',
  props<{ periodicelement: PeriodicElement }>()
);
