import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { PeriodicElement } from '@els/felis/api-periodicelements';
import * as PeriodicelementsActions from './periodicelements.actions';

export const PERIODICELEMENTS_FEATURE_KEY = 'periodicelements';

export interface State extends EntityState<PeriodicElement> {
  selectedId?: string | number; // which Periodicelements record has been selected
  loaded: boolean; // has the Periodicelements list been loaded
  error?: string | null; // last known error (if any)
}

export interface PeriodicelementsPartialState {
  readonly [PERIODICELEMENTS_FEATURE_KEY]: State;
}

export const periodicelementsAdapter: EntityAdapter<PeriodicElement> = createEntityAdapter<
  PeriodicElement
>();

export const initialState: State = periodicelementsAdapter.getInitialState({
  // set initial required properties
  loaded: false,
});

const periodicelementsReducer = createReducer(
  initialState,
  on(PeriodicelementsActions.loadPeriodicelements, state => ({
    ...state,
    loaded: false,
    error: null,
  })),

  on(
    PeriodicelementsActions.loadPeriodicelementsSuccess,
    (state, { periodicelements }) =>
      periodicelementsAdapter.setAll(periodicelements, {
        ...state,
        loaded: true,
      })
  ),

  on(
    PeriodicelementsActions.loadPeriodicelementsFailure,
    (state, { error }) => ({ ...state, error })
  ),

  on(PeriodicelementsActions.loadPeriodicelementSuccess, (state, action) => {
    return periodicelementsAdapter.setOne(action.periodicelement, state);
  })
);

export function reducer(state: State | undefined, action: Action) {
  return periodicelementsReducer(state, action);
}
