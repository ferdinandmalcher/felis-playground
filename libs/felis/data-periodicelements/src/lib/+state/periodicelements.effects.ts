import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { ofRoute, mapToParam } from '@els/shared/util-ngrx-router';

import { ElementsService } from '@els/felis/api-periodicelements';
import * as PeriodicelementsActions from './periodicelements.actions';
import { concatMap, map, catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import {
  createTransformDefinition,
  WindowActionsTransformer,
  onlyForMonitors,
  Monitors,
} from '@els/felis/data-cross-window';

@Injectable()
export class PeriodicelementsEffects {
  /**
   * transformation of inbound actions from other monitors
   */
  private inboundTransformations = [
    createTransformDefinition({
      from: PeriodicelementsActions.selectPeriodicelement,
      to: action =>
        PeriodicelementsActions.selectPeriodicelementBroadcast({
          id: action.id,
        }),
    }),
  ];

  inboundActions$ = this.wat.createInboundTransformationEffect(
    this.inboundTransformations
  );

  /**
   * outbound actions that should be broadcasted to all other monitors
   */
  outboundActions$ = this.wat.createOutboundBroadcastEffect([
    PeriodicelementsActions.selectPeriodicelement,
  ]);

  loadPeriodicelements$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PeriodicelementsActions.loadPeriodicelements),
      concatMap(() =>
        this.es.getAll().pipe(
          map(periodicelements =>
            PeriodicelementsActions.loadPeriodicelementsSuccess({
              periodicelements,
            })
          ),
          catchError(error =>
            of(PeriodicelementsActions.loadPeriodicelementsFailure({ error }))
          )
        )
      )
    )
  );

  loadPeriodicelement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PeriodicelementsActions.loadPeriodicelement),
      concatMap(({ id }) =>
        this.es.getSingle(id).pipe(
          map(periodicelement =>
            PeriodicelementsActions.loadPeriodicelementSuccess({
              periodicelement,
            })
          ),
          catchError(() => []) // TODO
        )
      )
    )
  );

  showElementDetails$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PeriodicelementsActions.selectPeriodicelementBroadcast),
        onlyForMonitors(Monitors.Secondary),
        tap(({ id }) => this.router.navigate(['/elements/details', id]))
      ),
    { dispatch: false }
  );

  showElementDetailsBig$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(PeriodicelementsActions.selectPeriodicelementBroadcast),
        onlyForMonitors(Monitors.Tertiary),
        tap(({ id }) => this.router.navigate(['/elements/detailsbig', id]))
      ),
    { dispatch: false }
  );

  loadElementOnRouting$ = createEffect(() =>
    this.actions$.pipe(
      ofRoute(['elements/details/:id', 'elements/detailsbig/:id']),
      mapToParam('id'),
      map(id => PeriodicelementsActions.loadPeriodicelement({ id }))
    )
  );

  constructor(
    private actions$: Actions,
    private wat: WindowActionsTransformer,
    private es: ElementsService,
    private router: Router
  ) {}
}
