import { Periodicelement } from '@els/felis/api-periodicelements';
import * as PeriodicelementsActions from './periodicelements.actions';
import { State, initialState, reducer } from './periodicelements.reducer';

describe('Periodicelements Reducer', () => {
  /* const createPeriodicelementsEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as Periodicelement);*/

  beforeEach(() => {});

  describe('valid Periodicelements actions', () => {
    it('loadPeriodicelementsSuccess should return set the list of known Periodicelements', () => {
      const periodicelements = [
        /* createPeriodicelementsEntity('PRODUCT-AAA'),
        createPeriodicelementsEntity('PRODUCT-zzz'),*/
      ];
      const action = PeriodicelementsActions.loadPeriodicelementsSuccess({
        periodicelements,
      });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
