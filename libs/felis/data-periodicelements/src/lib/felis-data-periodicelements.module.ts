import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromPeriodicelements from './+state/periodicelements.reducer';
import { PeriodicelementsEffects } from './+state/periodicelements.effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromPeriodicelements.PERIODICELEMENTS_FEATURE_KEY,
      fromPeriodicelements.reducer
    ),
    EffectsModule.forFeature([PeriodicelementsEffects]),
  ],
})
export class PeriodicelementsDataAccessModule {}
