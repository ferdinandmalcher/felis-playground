import { filter, map } from 'rxjs/operators';
import {
  BaseRouterStoreState,
  ROUTER_NAVIGATED,
  RouterAction,
  RouterNavigatedAction,
} from '@ngrx/router-store';
import { MonoTypeOperatorFunction, pipe, OperatorFunction } from 'rxjs';
import { CustomRouterStateSnapshot } from './serializer';

export function isRoute(route: string | string[] | RegExp) {
  return (
    action: RouterAction<
      RouterNavigatedAction<CustomRouterStateSnapshot>,
      CustomRouterStateSnapshot
    >
  ) => {
    const isRouteAction = action.type === ROUTER_NAVIGATED;
    if (isRouteAction) {
      const routePath = action.payload.routerState.path;
      if (Array.isArray(route)) {
        return route.indexOf(routePath) > -1;
      } else if (route instanceof RegExp) {
        return route.test(routePath);
      } else {
        return routePath === route;
      }
    }
    return isRouteAction;
  };
}

export function ofRoute<T extends BaseRouterStoreState>(
  route: string | string[] | RegExp
): MonoTypeOperatorFunction<RouterNavigatedAction<T>> {
  {
    return filter<RouterNavigatedAction<any>>(isRoute(route));
  }
}

export function mapToParams() {
  return map(
    (
      action: RouterAction<
        RouterNavigatedAction<CustomRouterStateSnapshot>,
        CustomRouterStateSnapshot
      >
    ) => action.payload.routerState.params
  );
}

export function mapToParam(
  paramName: string
): OperatorFunction<
  RouterAction<
    RouterNavigatedAction<CustomRouterStateSnapshot>,
    CustomRouterStateSnapshot
  >,
  string
> {
  return pipe(
    mapToParams(),
    map(params => params[paramName])
  );
}
