import { createFeatureSelector } from '@ngrx/store';
import {
  RouterReducerState,
  getSelectors,
  DEFAULT_ROUTER_FEATURENAME,
} from '@ngrx/router-store';

export const ROUTER_STATE_KEY = DEFAULT_ROUTER_FEATURENAME;

export const selectRouter = createFeatureSelector<RouterReducerState<any>>(
  ROUTER_STATE_KEY
);

export const {
  selectCurrentRoute,
  selectQueryParam,
  selectQueryParams,
  selectRouteData,
  selectRouteParam,
  selectRouteParams,
  selectUrl,
} = getSelectors(selectRouter);
