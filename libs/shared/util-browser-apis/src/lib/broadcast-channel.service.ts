import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';

/**
 * Abstraction of the native `BroadcactChannel` object.
 * Application services can inject this service to get access to a cross-window communication channel.
 * Other platforms like testing or Server-Side Rendering can provide a mock object of this service to avoid a dependency on a browser-native object.
 *
 * This only provides the basic messaging system.
 * Therefore, this service should never be used in components directly
 * but always through higher layers like NgRx.
 */
@Injectable({
  providedIn: 'root',
})
export class BroadcastChannelService {
  /**
   * Instance of the native `BroadcastChannel` for cross-window communication
   */
  private channel = new BroadcastChannel('felisActionsBroadcast');

  /**
   * Observable of raw messages received from the channel
   */
  onMessage$ = fromEvent<MessageEvent>(this.channel, 'message');

  /**
   * Sends the given message to other windows listening to the same channel.
   * @param message The message to send
   */
  postMessage(message: any) {
    return this.channel.postMessage(message);
  }
}
