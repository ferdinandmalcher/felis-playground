import { InjectionToken, inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

/**
 * A DI token that represents the native browser `window` object.
 * This is useful for testing (and other platforms) where `window` is not available.
 */
export const WINDOW = new InjectionToken<Window>('WindowToken', {
  factory: () => {
    const document = inject(DOCUMENT);
    if (document.defaultView) {
      return document.defaultView;
    } else {
      throw new Error('The window object is not available.');
    }
  },
});
