## Abstraktion nativer Browser-APIs

Der Browser bietet native Schnittstellen, die auf anderen Plattformen nicht zur Verfügung stehen.
Zum Beispiel können die nativen Objekte `window` oder `document` beim Testing mit Jest oder beim Server-Side Rendering nicht genutzt werden.

Es wird deshalb eine Abstraktion dieser Objekte genutzt, die über Dependency Injection in der Anwendung angefordert werden kann.
Für `document` existiert bereits ein DI-Token von Angular.

> Native Schnittstellen des Browsers werden niemals direkt genutzt, sondern immer durch eine Abstraktionsschicht.

```ts
import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { WINDOW } from '@els/shared/util-browser-apis';

@Injectable({ providedIn: 'root' })
export class MyService {
  constructor(
    @Inject(WINDOW) private window: Window,
    @Inject(DOCUMENT) private document: Document
  ) {}

  myMethod() {
    // Verwendung
    this.window.open();
    const title = this.document.title;
  }
}
```

## Provider für `WINDOW`

Damit `WINDOW` im Test verwendet werden kann, muss ein Mock-Objekt providet werden:

```ts
// x.spec.ts
const windowMock: Partial<Window>;

beforeEach(() => {
  windowMock = {
    open: jest.fn(),
  };

  TestBed.configureTestingModule({
    providers: [{ provide: WINDOW, useValue: windowMock }],
  });
});
```

## BroadcastChannel

Zur Kommunikation zwischen den Fenstern wird ein `BroadcastChannel` verwendet.
Damit die Anwendung nicht von diesem nativen Objekt abhängig ist, existiert der `BroadcastChannelService` zur Abstraktion.
Er definiert einen Channel mit einem statischen Namen. Da ein Service in Angular grundsätzlich ein Singleton ist, kann jeder Teil der Anwendung den Service injizieren und dieselbe Instanz nutzen.

### Gründe für die Abstraktion

- **Abhängigkeit vermeiden:** Der `BroadcastChannelService` kann beim Testing durch einen Mock ersetzt werden.
- **Austauschbarkeit:** Der `BroadcastChannel` funktioniert nur in modernen Browsern. Falls nötig, könnte der Service später eine andere Technologie nutzen, solange die Schnittstelle dieselbe bleibt.
