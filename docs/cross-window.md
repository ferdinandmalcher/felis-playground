# Cross-Window Communication

Die Anwendung wird in mehreren Fenstern parallel betrieben. Das heißt, mehrere Instanzen derselben Applikation sind gleichzeitig aktiv.
Aus Nutzersicht ist "die Anwendung" die Gesamtheit aller aktiven Fenster.
Zwischen den Fenstern findet Kommunikation statt, sodass eine Aktion sich stets auf den Zustand der anderen Fenster auswirken kann.

Zur Kommunikation zwischen den aktiven Fenstern wird ein lokaler Event-Bus verwendet, auf den alle Fenster zugreifen können: ein Broadcast Channel.

## Datenfluss

![Data Flow](./dataflow-cross-window.svg)

Das Framework NgRx erzeugt einen anwendungsglobalen Strom von Actions. Diese Actions werden in Reducern verarbeitet, um einen neuen State zu berechnen.
Sie können außerdem in Effects genutzt werden, um Seiteneffekte auszuführen oder auf andere Actions zu mappen.

In der Anwendung existiert eine Festlegung, welche NgRx-Actions über alle aktiven Fenster verteilt ("gebroadcastet") werden sollen.
Ausgehend vom Datenstrom _aller_ Actions werden mithilfe eines Effects die _gewünschten_ Actions an den fensterübergreifenden Broadcast Channel übermittelt (_Outbound_).

Der Broadcast Channel spielt die Nachrichten an alle Abonnenten aus, aber **nicht** an das Fenster, das die Nachricht gesendet hat.

Jede _andere_ aktive Anwendung verarbeitet die eingehenden globalen Ereignisse in einem Effect (_Inbound_).

Dieser Effect entscheidet für jede aus dem Broadcast Channel eingehende Action, …

- ob sie für diese Anwendung auf diesem Monitor überhaupt verarbeitet werden soll, und
- welche andere Action in der Folge ausgeführt wird.

Im Beispiel wird in Anwendung B die eingehende Action `selectElement` in die Folgeaktion `selectElementBroadcast` umgewandelt.
Diese wird wieder in den Store dispatcht und taucht damit im anwendungsglobalen Action-Strom von B auf.

Diese Umwandlung in eine neue Action mit dem Suffix `Broadcast` ist notwendig, damit kein Kreislauf entsteht:

> _Anwendung A sendet `selectElement` an den Broadcast Channel. B empfängt diese Action und dispatcht daraufhin ebenfalls `selectElement`, das über den Broadcast Channel an A gesendet wird. Durch die eingehende Action beginnt der Prozess in A von vorn, und es entsteht ein Kreislauf._

## Implementierung

Alle allgemeinen Bestandteile für die Cross-Window Communication sind in der Bibliothek `felis-data-cross-window` untergebracht.

Der `CrossWindowService` nutzt die native [Broadcast Channel API](https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API) des Browsers, um einen fensterübergreifenden Kommmunikationskanel zu etablieren.
Alle Nachrichten werden an **alle anderen** Fenster ausgespielt.

Die eingehenden Nachrichten werden vom Service im Property `inboundBroadcastActions$` als Observable zur Verfügung gestellt.

Um eine Abhängigkeit auf das native Objekt zu vermeiden, wird der `BroadcastChannel` nur mithilfe der Abstraktion `BroadcastChannelService` genutzt.

### Monitor-Identifikation

Die Methode `openApplicationWindows()` im `CrossWindowService` öffnet alle Fenster der Anwendung.

> ⚠️ In jedem geöffneten Fenster läuft die exakt selbe Anwendung, aber in verschiedenen Zuständen. Jede Instanz besitzt einen eigenen NgRx-Store mit einem eigenen State. Jede Instanz muss eigenständig Daten vom Server nachladen.

Ein geöffnetes aktives Anwendungsfenster wird hier auch _Monitor_ genannt.
Zum Öffnen wird die native Methode `window.open()` genutzt.
Dabei erhält jedes Fenster einen Identifikator.
In einer aktiven Anwendung kann der Identifikator des eigenen Fensters mit `window.name` ausgelesen werden.
Damit wird später entschieden, welche eingehende Nachricht welche Aktion auf welchem Monitor auslöst.

Die Bibliothek stellt den Operator `onlyForMonitors()` zur Verfügung.
Damit kann später im Effect der Action-Strom gefiltert werden, um die Actions nur zu verarbeiten, wenn die Anwendung auf einem bestimmten Monitor läuft:

```ts
showElementDetails$ = createEffect(() =>
  this.actions$.pipe(
    ofType(selectElementBroadcast),
    onlyForMonitors(Monitors.Secondary, Monitors.Tertiary)
    // ...
  )
);
```

### Action-Transformation

Alle Bestandteile von NgRx sind in `data`-Bibliotheken organisiert, die nach Features oder fachlichen Aspekten getrennt sind.
Es kann daher keine globale Behandlung aller Broadcast-Actions geben – weder für Outbound noch für Inbound.

Jede `data`-Library muss also eigene Effects definieren, um ausgehende und eingehende Nachrichten aus dem Broadcast Channel zu behandeln.
Der Service `WindowActionsTransformer` stellt dafür Helfermethoden zur Verfügung.

#### Outbound Actions

Jede `data`-Bibliothek, die einen Slice des NgRx-States verwaltet, muss selbst definieren, welche Actions aus ihrem Bereich an den Broadcast Channel ausgespielt werden sollen.
Dafür wird jeweils der folgende Effect `outboundActions$` genutzt.
Die Methode `createOutboundBroadcastEffect()` erhält als Argument ein Array von allen Actions, die über den Broadcast Channel verteilt werden sollen.

```ts
@Injectable()
export class MyFeatureEffects {
  outboundActions$ = this.wat.createOutboundBroadcastEffect([
    PeriodicelementsActions.selectPeriodicelement,
  ]);

  constructor(private wat: WindowActionsTransformer /* ... */) {}
}
```

#### Inbound Actions

Jede `data`-Bibliothek legt selbst fest, wie die eingehenden Nachrichten behandelt werden und in welche Folgeaktionen sie umgewandelt werden.

Eine einzelne Transformation wird mit der Funktion `createTransformDefinition()` definiert:

```ts
const transformDef = createTransformDefinition({
  from: selectElement,
  to: action => selectElementBroadcast({ id: action.id }),
});
```

- `from`: die eingehende Action
- `to`: Factory-Funktion für die daraus zu erzeugende neue Action

Um die korrekte Typisierung zu ermöglichen, sollten im Effect zunächst alle Transformationsvorschriften in einem Array gespeichert werden.
Anschließend wird mithilfe von `createInboundTransformationEffect()` ein Effect erzeugt, der die eingehenden Nachrichten aus dem Broadcast Channel nach den festgelegten Vorschriften in neue Actions transformiert:

```ts
private inboundTransformations = [
  createTransformDefinition({
    from: selectElement,
    to: action => selectElementBroadcast({ id: action.id }),
  })
];

inboundActions$ = this.wat.createInboundTransformationEffect(
  this.inboundTransformations
);
```

## Aktionen ausführen für eingehende Nachrichten

Sind die Effects korrekt aufgesetzt und konfiguriert, wird jede eingehende Nachricht aus dem Broadcast Channel als neue Action in das NgRx-System dispatcht.

> Jede aktive Anwendung erhält stets **alle** Broadcast-Actions und führt **alle** Transformationen aus – mit Ausnahme der Anwendung, von der die Action stammt.

Für die eingehenden Actions sollen auf verschiedenen Monitoren verschiedene Aktionen ausgelöst werden.
Dazu werden weitere Effects entwickelt.

Mit dem Operator `onlyForMonitors()` wird die Effect-Pipeline nur dann weitergeführt, wenn sie auf einem der angegebenen Monitore ausgeführt wird.
So können für einzelne Monitore unterschiedliche Aktionen angestoßen werden.

```ts
// Route wechseln (nur Monitor 2)
showElementDetails$ = createEffect(
  () =>
    this.actions$.pipe(
      ofType(selectElementBroadcast),
      onlyForMonitors(Monitors.Secondary),
      tap(action => this.router.navigate(['/details', action.id]))
    ),
  { dispatch: false }
);

// andere Action auslösen (nur Monitor 3)
loadElementWhenSelected$ = createEffect(() =>
  this.actions$.pipe(
    ofType(selectElementBroadcast),
    onlyForMonitors(Monitors.Tertiary),
    map(action => loadElement({ id: action.id }))
  )
);
```
