# Projektstruktur, Bibliotheken und Kommandos

Nx definiert _Applications_ und _Libraries_ als elementare Architekturbausteine.
Sie werden jeweils als unabhängige Unterprojekte in den Ordnern `apps` und `libs` organisiert.

Eine Application sollte dabei nur den nötigen Rahmen zur Verfügung stellen, um eine Library zu konsumieren.
Alle weiteren technischen Aspekte werden in Librarys untergebracht.

Die Architektur orientiert sich grundsätzlich an den Vorschlägen aus der Nx-Dokumentation.

> ⚠️ Die hier notierten Konventionen sind nicht in Stein gemeißelt. Dies ist ein _Living Document_: Ergeben sich im Projekt neue Erkenntnisse, soll das Dokument ergänzt werden.

## Quick Guide: Feature anlegen

Situation: Es wird ein neues logisches Feature benötigt, das einen eigenen Teil des NgRx-States verwaltet, eine API-Anbindung besitzt und Komponenten/Seiten für die Anwendung anbietet.

- Bibliotheken anlegen
  - `feature`
  - `api`
  - `data`
- NgRx einrichten in `data`-Bibliothek
- Erste Komponenten (und ggf. Routen) anlegen in der `feature`-Bibliothek

## Konventionen/Best Practices

- **Barrel File:** Die öffentliche Schnittstelle der Bibliothek wird durch die `index.ts` definiert.
- **Import-Pfad:** Eine Bibliothek wird immer aus ihrem Alias-Pfad importiert, z.B. `@els/felis/feature-foo`, aber niemals mit relativen Pfaden direkt aus dem Dateisystem.
- **Bibliothekstyp:** Der Bibliothekstyp wird…
  - im Ordnernamen angegeben, z.B. `feature-foo` oder `data-foobar`.
  - als Tag mit dem Präfix `type` festgelegt (`nx.json`).
  - im Selektor-Präfix angegeben, siehe Abschnitt "Selektor-Präfix"
- **Constraints:** Es werden Constraints definiert (`tslint.json`). Sie legen fest, welche Bibliotheken voneinander abhängen dürfen.

## Selektor-Präfix

Das Selektor-Präfix sollte die Zugehörigkeit zur Bibliothek und fachlichen Domäne ausdrücken.
Ein Präfix ist nur für Komponenten, Pipes und Direktiven relevant.
Feature-Libraries geben keinen Library-Typ in ihrem Präfix an.
Der `slug` ist ein frei gewähltes Kürzel, das auf den Bibliotheksnamen hindeutet.
Ein Präfix sollte eindeutig im gesamten Projekt sein.

> Feature: **`<app>-<slug>`**

> andere: **`<app>-<libtype>-<slug>`**

### Beispiele

| **Bibliothek**         | **Präfix**        |
| ---------------------- | ----------------- |
| `felis/feature-foobar` | `felis-fb`        |
| `felis/ui-foobar`      | `felis-ui-fb`     |
| `felis/util-testing`   | `felis-util-test` |
| `shared/ui-baz`        | `sh-ui-baz`       |
| `shared/util-rxjs`     | `sh-util-rxjs`    |

## Typen von Bibliotheken

Nx definiert [verschiedene Typen von Bibliotheken](https://nx.dev/angular/workspace/structure/library-types). Auf dieser Grundlage wurden in diesem Projekt die folgenden Festlegungen getroffen.

### `api`: API-Anbindung

> API libraries contain code that function as client-side delegate layers to server tier APIs.

- enthält Services und Server API Models (DTO)
- Models generiert aus OpenAPI/Swagger
- eine API-Lib für jeden Microservice (?)
- Constraints: darf **nur** importieren von `util` und `api`
- Tag: `type:api`

```bash
# API-Bibliothek generieren
nx g lib felis/api-FOO --tags=scope:felis,type:api --defaults
```

### `data`: Datenzugriff

> Data-access libs contain files related to state management.

- NgRx (im Unterordner `+state`)
- Re-Export von Server API Models
- Client Models (falls abweichend von Server API Models)
- Constraints: darf **nur** importieren von `data`, `util` und `api`
- Tag: `type:data`

```bash
# Data-Bibliothek generieren
nx g lib felis/data-FOO --tags=scope:felis,type:data --defaults

# NgRx in die Bibliothek integrieren
nx g ngrx FOO --module libs/felis/data-FOO/src/lib/felis-data-FOO.module.ts --directory +state --defaults
```

### `feature`: Fachliches Feature

> A feature library contains a set of files that configure a business use case or a page in an application. Most of the components in such a library are smart components that interact with data sources. This type of library also contains most of the UI logic, form validation code, etc. Feature libraries are almost always app-specific and are often lazy-loaded.

- Hauptbibliothek für ein fachliches Feature
- spezifische Komponenten und Seiten der Anwendung
- Routing (deshalb immer Optionen `--routing` und ggf. `--lazy` verwenden)
- enthält kein State Management, sondern importiert dafür aus `data`-Bibliotheken
- reine Presentational Components auslagern in `ui`-Bibliothek
- Constraints: darf **nicht** importieren von `api`
- Tag: `type:feature`

```bash
# Feature-Bibliothek generieren
nx g lib felis/feature-FOO --prefix=felis-foo --tags=scope:felis,type:feature --style=scss --routing --lazy
```

### `ui`: Oberflächenkomponenten

> A UI library is a collection of related presentational components. There are generally no services injected into these components (all of the data they need should come from Inputs).

- enthält nur Presentational Components / Dumb Components
- Constraints: darf **nur** importieren von `ui` und `util`
- Tag: `type:ui`

```bash
# UI-Bibliothek generieren
nx g lib felis/ui-FOO --prefix=felis-ui-foo --tags=scope:felis,type:ui --style=scss
```

### `util`: Helferfunktionen

> A utility library contains low level code used by many libraries. Often there is no framework-specific code and the library is simply a collection of utilities or pure functions.

- Helferfunktionen usw.
- enthält ggf. kein `NgModule`, wenn nur einfache Funktionen exportiert werden
- Constraints: darf **nur** importieren von `util`
- Tag: `type:util`

```bash
# Utility-Bibliothek generieren
nx g lib felis/util-FOO --tags=scope:felis,type:util --defaults
```

## Struktur in `libs`

Die [Nx-Dokumentation](https://nx.dev/angular/guides/monorepo-nx-enterprise#code-organization-amp-naming-conventions) schlägt verschiedene Ansätze zur Organisation des `libs`-Ordners vor.

> It's a good convention to put applications-specific libraries into the directory matching the application name. [...]
> For larger projects, it is a good idea to group libraries into application sections.

Es ist davon auszugehen, dass perspektivisch mehrere Applications im Workspace verwaltet werden.
Anwendungsspezifische Libs sollten also nach der zugehörigen Anwendung gruppiert werden.

Es sollte zunächst eine flache Hierarchie gepflegt werden mit Gruppierung anhand der Anwendung.
Der globale Ordner `shared` bündelt Bibliotheken, die für alle Anwendungen zur Verfügung stehen.

```
els/
├── apps/
│   ├── felis/
│   ├── felis-e2e/
│   ├── lupus/
│   └── lupus-e2e/
├── libs/
│   ├── felis/
│   │   ├── feature-foobar/
│   │   ├── data-foobar/
│   │   └── api-foobar/
│   ├── lupus/
│   │   ├── feature-einsaetze/
│   │   ├── feature-login/
│   │   ├── ui-einsaetze
│   │   └── utils-einsaetze/
│   └── shared/
│       ├── ui/
│       └── utils-testing/
└── ...
```

Zeichnen sich in der fachlichen Domäne klare Grenzen ab, sollten diese durch weitere Gruppierungen ausgedrückt werden.
Im folgenden Beispiel sind `registration` und `search` Teile der Domäne, `shared` enthält Teile, die über die Domänengrenzen hinweg zur Verfügung stehen, aber dennoch spezifisch für die Anwendung sind.

```
els/
├── apps/
├── libs/
│   ├── felis/
│   │    ├── registration/
│   │    │   ├── feature-main/
│   │    │   ├── feature-login/
│   │    │   ├── ui-form/
│   │    │   └── utils-testing/
│   │    ├── search/
│   │    │   ├── feature-results/
│   │    │   └── utils-testing/
│   │    └── shared/
│   │        └── ui/
│   ├── lupus/
|   └── shared/
│       ├── ui/
│       └── utils-testing/
└── ...
```

Die fachliche Zugehörigkeit sollte durch Tags mit dem Präfix `scope` ausgedrückt werden.
Dazu gibt es noch keine gefestigten Erkenntnisse.

## Weitere Kommandos

### Dependency Graph aller Bibliotheken und Apps

```bash
nx dep-graph
```

### Bibliothek löschen

Bibliotheken niemals von Hand löschen, weil dann auch alle Referenzen und Aliase händisch entfernt werden müssen. Stattdessen den Löschbefehl verwenden:

```bash
nx g remove felis-feature-FOO
```

### Bibliothek verschieben/umbenennen

Bibliotheken niemals von Hand umbenennen, weil dann auch alle Referenzen händisch angepasst werden müssen. Stattdessen den Befehl zum Verschieben verwenden:

```bash
nx g move --project felis-feature-FOO --destination felis/feature-BAR
```

## Ressourcen

- [Nx Docs: Creating libraries](https://nx.dev/angular/workspace/structure/creating-libraries)
- [Nx Docs: Library types](https://nx.dev/angular/workspace/structure/library-types)
- [Nx Docs: Using tags](https://nx.dev/angular/workspace/structure/monorepo-tags)
- [Nx Docs: library schematics](https://nx.dev/angular/plugins/angular/schematics/library)
- [Nx Docs: ngrx schematics](https://nx.dev/angular/plugins/angular/schematics/ngrx)
- [Nx Docs: Using Nx at Enterprises](https://nx.dev/angular/guides/monorepo-nx-enterprise)
