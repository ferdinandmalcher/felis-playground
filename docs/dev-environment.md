# Entwicklungsumgebung

Als Umgebung für die Angular-Entwicklung wird _Visual Studio Code_ empfohlen.

## Extensions

### notwendig

Die notwendigen Extensions sind als _Recommendations_ in der Datei `.vscode/extensions.json` hinterlegt.
VS Code schlägt dann beim Start automatisch vor, diese Extensions zu installieren.

| **Extension**             | **ID**                                                                                                                                       | **Beschreibung**                                                                  |
| ------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| Angular Language Service  | [`Angular.ng-template`](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)                                             | Type Checking in Angular-Templates                                                |
| EditorConfig for VS Code  | [`EditorConfig.EditorConfig`](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)                                 | Projektbezogene Editor-Konfiguration, wird aus der Datei `.editorconfig` bezogen. |
| Prettier - Code formatter | [`esbenp.prettier-vscode`](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)                                       | Code-Formatierung                                                                 |
| TSLint                    | [`ms-vscode.vscode-typescript-tslint-plugin`](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin) | Linter – prüft Code nach festgelegten Regeln                                      |

### nützlich/empfohlen

Die folgenden Extensions sind nicht notwendig, können aber bei der Entwicklung nützlich sein.

| **Extension**              | **ID**                                                                                                                 | **Beschreibung**                                            |
| -------------------------- | ---------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------- |
| vscode-icons               | [`vscode-icons-team.vscode-icons`](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons) | Icons für den Dateibaum, mit spezifischen Icons für Angular |
| Nx Console                 | [`nrwl.angular-console`](https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console)                     | UI für Nrwl Nx                                              |
| GitLens — Git supercharged | [`eamodio.gitlens`](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)                               | Git-Integration direkt im Code                              |
| Jest                       | [`Orta.vscode-jest`](https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest)                             | Testausführung direkt im Editor                             |
|                            |
