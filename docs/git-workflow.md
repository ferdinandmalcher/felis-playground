# Git-Workflow

## Branching

- Hauptbranch mit stabilen Features ist `develop`.
- Release-Branch ist `master`. Merge in `master` nur von `develop`
- Feature-Commits werden in der Regel auf Branches angelegt, nicht direkt auf `develop`
- Jedes Feature (oder sonstige Änderung) wird in einem eigenen Branch organisiert.
- Branchname
  - Präfix `feature`, `bugfix`, `hotfix`, …
  - JIRA-Ticket-ID
  - Beispiel: `feature/FELISNG-84_multi-window`
  - automatisch generieren lassen in JIRA?
- Commit Messages auf Feature-Branches müssen zunächst nicht der Konvention entsprechen, siehe unten.
- vor dem Merge
  - Review (andere Person oder Selbst-Review)
  - mit `develop` angleichen
    - Merge `develop` > `feature`, dann Squash nötig vor dem Merge in `develop`
    - ODER Rebase `feature` gegen `develop` für lineare Historie und Fast-Forward Merge. Vorher ggf. Historie bereinigen (durch Interactive Rebase)

## Gründe für Branching-Workflow

- **Ein Feature-Branch ist eine Spielwiese:**
  - Dinge ausprobieren, verwerfen, überarbeiten, experimentieren!
  - Unsaubere Commit Messages möglich
  - kaputte Features
  - Hier ist alles erlaubt!
- **Der `develop` ist _keine_ Spielwiese:**
  - Gemeinsame lineare Historie aller Entwickler.
  - Sie soll nachvollziehbar und sauber sein, ohne kurzfristige Experimente oder Merge Commits.
  - Enthält nur funktionierende Versionsstände.
  - Es sind nur die Commits enthalten, die die Implementierung und das Review durchlaufen haben.

## Pull Request (PR)

- Für jeden Feature-Branch, der irgendwann gemerget werden soll, wird nach Anlegen ein _Pull Request_ im BitBucket erstellt.
- Solange der Pull Request nicht final ist, wird er auf _"WIP"_ gesetzt.

## Review

- Review für Pull Request im BitBucket durchführen (Kommentare)
- Zwecke des Reviews:
  - Vier-Augen-Prinzip
  - Wissenstransfer
  - einheitlicher Code-Stil
  - Onboarding anderer Teammitglieder

## Merge

- aussagekräftige Historie auf dem Feature-Branch erzeugen. Varianten:
  - **implizit:** Commits werden von vornherein in einer sauberen Historie angelegt
  - **zusammenhängendes Feature:** Squash (alle Commits zusammenschmelzen)
  - **mehrere einzelne Themen auf dem Branch (seltener):** Interactive Rebase (Historie rückwirkend neu organisieren, dabei z. B. Commits squashen, umbenennen oder umsortieren)
- **Rebase gegen** `develop`: siehe separater Abschnitt
- finalen Merge für den PR immer im BitBucket durchführen, nicht lokal im SourceTree

## Commit Messages

> Auf Feature-Branches muss die Konvention nicht verfolgt werden, wenn vor dem Merge die Historie bereinigt wird (meist durch Squash). Folgendes gilt also nur für Commits auf `develop`.

- JIRA-Ticket-ID angeben
- Nachricht auf Deutsch, i.d.R. Name des Tickets
- separate Commits für Teilaspekte eines Tickets sind möglich
- Titel der Message kurz und bündig halten
- weitere Informationen im Body unterbringen, nach einer Leerzeile
- aktive Verben im Infinitiv verwenden, siehe Beispiele

### Beispiele

#### mit Ticket-Name

```
[FELISNG-100] Konzept für Multi-Window Communication
```

```
[FELISNG-110] Listenansicht für Einsatzmittel
```

### Atomare Commits für Teilaspekte

```
[FELISNG-84] Projektstruktur mit Nx aufsetzen
```

```
[FELISNG-112] Feature-Libs neu erstellen

War nötig wegen falscher Pfade in der verschobenen Library.
Nach Neuanlegen stimmen die Pfade wieder.
```

### Merge `develop` in `feature`, danach Squash

Wird vor dem Merge ein Squash durchgeführt, muss kein Rebase gemacht werden.
Es reicht dann aus, vorher den aktuellen `develop` in `feature` zu mergen, um die Inhalte von `develop` zu erhalten.

Dabei können Konflikte auftauchen, die behoben werden müssen. Es entsteht ein Merge-Commit, der später aber durch den Squash nicht mehr sichtbar sein wird.

```bash
# aktuellen Develop vom Server holen
git checkout develop
git pull

# auf Feature-Branch wechseln und Develop hineinmergen
git checkout feature
git merge develop
```

Danach kann der Feature-Branch über die UI von BitBucket gesquasht und gemerget werden.

## Rebase `feature` gegen `develop`

Sind auf dem Feature-Branch mehrere Commits, die erhalten bleiben sollen (kein Squash), muss ein Rebase durchgeführt werden.

Ein Rebase verschiebt Commits in der Historie. Führt man einen Rebase von `feature` gegen `develop` durch, so werden die Commits von `feature` **hinter** denen von `develop` angeordnet. Das entspricht inhaltlich einem Merge von `develop` nach `feature`, aber: Die Historie wird linear, als wären die Commits von `feature` erst zeitlich **nach** denen von `develop` entstanden. Es entsteht kein Merge-Commit.

Beim Rebase können Konflikte auftauchen, die aufzulösen sind. Anschließend ist immer ein konfliktfreier _Fast-Forward Merge_ möglich. So entsteht eine lineare Historie ohne Abzweigungen und Merge Commits.

Ein Rebase verändert die Historie! Danach ist immer ein Force-Push auf den Feature-Branch nötig. Der darf nur durchgeführt werden, wenn niemand mehr diesen Branch verwendet – also kurz vor dem Merge.

```bash
# aktuellen Develop vom Server holen
git checkout develop
git pull

# auf Feature-Branch wechseln und gegen Develop rebasen
git checkout feature
git rebase develop

# alternativ: Interactive Rebase
git rebase -i develop
```

Anschließend kann der Branch über die UI von BitBucket gemerget werden (Fast-Forward, **kein Squash!**).
