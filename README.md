# ELS

## Start

Das Projekt muss zunächst installiert werden, anschließend können einzelne Anwendungen gestartet werden.
Die Nx CLI muss dazu global installiert sein.

```bash
# Nx CLI installieren
npm install -g nx
```

```bash
# im Projektordner: Abhängigkeiten installieren
npm install

# App starten
nx serve felis
```
