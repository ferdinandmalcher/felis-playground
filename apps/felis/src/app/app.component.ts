import { Component } from '@angular/core';
import {
  CrossWindowService,
  openApplicationWindows,
} from '@els/felis/data-cross-window';
import { Store } from '@ngrx/store';

@Component({
  selector: 'felis-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private cws: CrossWindowService, private store: Store) {
    console.log('my window name:', window.name);
  }

  openApplicationWindows() {
    this.store.dispatch(openApplicationWindows());
  }
}
