import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreRouterConnectingModule, routerReducer } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  CustomRouterStateSerializer,
  ROUTER_STATE_KEY,
} from '@els/shared/util-ngrx-router';

import {
  CrossWindowDataAccessModule,
  MonitorEntryGuard,
} from '@els/felis/data-cross-window';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { PeriodicelementsDataAccessModule } from '@els/felis/data-periodicelements';
import { WINDOW } from '@els/shared/util-browser-apis';

const routes: Routes = [
  {
    path: 'monitor/:monitorName',
    canActivate: [MonitorEntryGuard],
    children: [],
  },
  {
    path: 'elements',
    loadChildren: () =>
      import('@els/felis/feature-periodicelements').then(
        m => m.FelisFeaturePeriodicelementsModule
      ),
  },
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CrossWindowDataAccessModule,
    PeriodicelementsDataAccessModule,
    RouterModule.forRoot(routes, { initialNavigation: 'enabled' }),
    StoreRouterConnectingModule.forRoot({
      serializer: CustomRouterStateSerializer,
      stateKey: ROUTER_STATE_KEY,
    }),
    StoreModule.forRoot(
      { [ROUTER_STATE_KEY]: routerReducer },
      {
        metaReducers: !environment.production ? [] : [],
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
        },
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
